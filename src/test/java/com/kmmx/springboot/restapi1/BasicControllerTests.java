package com.kmmx.springboot.restapi1;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * https://spring.io/guides/gs/testing-web/
 * https://www.baeldung.com/spring-boot-testing
 */
@SpringBootTest
class BasicControllerTests {
	@Test
	void contextLoads() {
		System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& TEST &&&&&&&&&&&&&&&&&");
		System.out.println(">>>>>>BasicControllerTests");
		System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");		
	}
}
