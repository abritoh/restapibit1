package com.kmmx.springboot.restapi1;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * https://spring.io/guides/gs/testing-web/
 */
@SpringBootTest
class Restapi1ApplicationTests {
	@Test
	void contextLoads() {
		System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& TEST &&&&&&&&&&&&&&&&&");
		System.out.println(">>>>>>Restapi1ApplicationTests");
		System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");		
	}
}
