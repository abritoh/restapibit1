package com.kmmx.springboot.restapi1;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class BasicController {  
  @GetMapping("/getAppName")
  @ResponseBody
  public String getAppName() {
    return "REST-API/n";
  }
}

