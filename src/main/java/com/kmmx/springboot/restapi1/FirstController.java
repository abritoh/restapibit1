package com.kmmx.springboot.restapi1;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ArrayList;

import com.kmmx.springboot.restapi1.dto.FrameworkDTO;
import java.time.LocalDateTime;

@RestController
public class FirstController {
	@RequestMapping("/")
	public String index() {
		return "Hello World from Spring Boot! \n";
  }
    
  @RequestMapping("/getDate")
  public String getDate() {
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd-HH:mm:ss");
    return dtf.format(LocalDateTime.now()) + "\n";
  }  

  @RequestMapping("/getFrameworks")
  public List<FrameworkDTO> getFrameworks() {
    List<FrameworkDTO> list = new ArrayList<>();    
    list.add(new FrameworkDTO("Spring", "Java"));
    list.add(new FrameworkDTO("SpringBoot", "Java"));
    list.add(new FrameworkDTO("Struts", "Java"));
    list.add(new FrameworkDTO("Strust2", "Java"));
    list.add(new FrameworkDTO("JAvaServerFaces", "Java"));
    list.add(new FrameworkDTO("AngularJS", "Javascript"));
    list.add(new FrameworkDTO("MVC5", "C#"));
    list.add(new FrameworkDTO("Django", "Python"));
    return list;
  }
}