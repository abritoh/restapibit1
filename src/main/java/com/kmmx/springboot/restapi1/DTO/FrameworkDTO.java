package com.kmmx.springboot.restapi1.dto;

public class FrameworkDTO {
    private String framework;
    private String language;

    public FrameworkDTO (String _framework, String _language) {
        framework = _framework;
        language = _language;
    }

    public String getFramework() {
        return framework;
    }
    public void setFramework(String value) {
        framework = value;
    }
    
    public String getLanguage() {
        return language;
    }
    public void setLanguage(String value) {
        language = value;
    }    
} 

