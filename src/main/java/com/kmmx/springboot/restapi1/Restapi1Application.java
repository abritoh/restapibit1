package com.kmmx.springboot.restapi1;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

/**
 * https://spring.io/guides/gs/spring-boot/
 **/
@SpringBootApplication
public class Restapi1Application {

	public static void main(String[] args) {
		SpringApplication.run(Restapi1Application.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {			
			System.out.println("**************************** START Listing BEANS ****");			
			String[] beanNames = ctx.getBeanDefinitionNames();
			Arrays.sort(beanNames);
			for (String beanName : beanNames) {
				System.out.println(beanName);
			}
			System.out.println("**************************** END Listing BEANS ******");
		};
	}
}
